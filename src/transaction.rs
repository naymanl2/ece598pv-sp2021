use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use ring::{digest};
use rand::Rng;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    //input and output
    input: [u8; 32],
    output: [u8; 32]
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
    transaction: Transaction,
    pub_key: [u8; 32],
    signature: [u8; 32]
}
// #[derive(Eq, PartialEq, Serialize, Deserialize, Clone, Hash, Default, Copy)]
// pub struct SignedTransaction{pub_key:[u8; 32]} // big endian u256


/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    // unimplemented!()
    let ser: Vec<u8> = bincode::serialize(&t).unwrap();
    let hashed = digest::digest(&digest::SHA256, &ser);
    return key.sign(hashed.as_ref());
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    // unimplemented!()
    // ring::signature::verify()
    let ser: Vec<u8> = bincode::serialize(&t).unwrap();
    let hashed = digest::digest(&digest::SHA256, &ser);

    /* reference verify in ring example */
    let peer_public_key =
    ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key);
    peer_public_key.verify(hashed.as_ref(), signature.as_ref()).is_ok()

}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let rand_in = rand::thread_rng().gen::<[u8; 32]>();
        let rand_out = rand::thread_rng().gen::<[u8; 32]>();
        return Transaction{input:rand_in, output:rand_out};
        //unimplemented!()
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
